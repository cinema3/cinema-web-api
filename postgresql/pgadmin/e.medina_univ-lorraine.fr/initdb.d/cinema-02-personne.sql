\connect iutsd;

CREATE TABLE personne
(
  id uuid DEFAULT gen_random_uuid() NOT NULL PRIMARY KEY,
  nom character varying(30),
  prenom character varying(30),
  naissance date,
  deces date,
  nationalites character(2)[],
  artiste character varying(30),
  creation timestamp with time zone DEFAULT now(),
  modification timestamp with time zone
);

ALTER TABLE IF EXISTS personne OWNER TO iutsd;

\COPY personne FROM '/tmp/cinema-personne.csv' DELIMITER ',' CSV HEADER QUOTE '"' ESCAPE '''' ENCODING 'UTF8';
