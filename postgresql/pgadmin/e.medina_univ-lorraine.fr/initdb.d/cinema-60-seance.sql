\connect iutsd;

CREATE TABLE cinema.seance (
  "id" integer PRIMARY KEY GENERATED BY DEFAULT AS IDENTITY,
  "film" uuid NOT NULL,
  "salle" integer NOT NULL,
  "seance" timestamp with time zone NOT NULL
);

ALTER TABLE cinema.seance
  ADD CONSTRAINT seance_film_fk FOREIGN KEY (film)
  REFERENCES cinema.film (id) MATCH SIMPLE
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
  NOT VALID;

CREATE INDEX seance_film_fki
  ON cinema.seance(film);

ALTER TABLE cinema.seance
  ADD CONSTRAINT seance_salle_fk FOREIGN KEY (salle)
  REFERENCES cinema.salle (id) MATCH SIMPLE
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
  NOT VALID;

CREATE INDEX seance_salle_fki
  ON cinema.seance(salle);

\COPY cinema.seance FROM '/tmp/60-seance.csv' DELIMITER ',' CSV HEADER QUOTE '"' ESCAPE '''' ENCODING 'UTF8';
