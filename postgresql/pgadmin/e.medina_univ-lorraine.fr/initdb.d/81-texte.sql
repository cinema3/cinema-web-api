CREATE TABLE IF NOT EXISTS public.texte
(
  id uuid NOT NULL,
  langue character(3) COLLATE pg_catalog."default" NOT NULL,
  texte text COLLATE pg_catalog."default",

  CONSTRAINT texte_personne_fk FOREIGN KEY (id)
	REFERENCES personne(id),

  CONSTRAINT texte_langue_fk FOREIGN KEY (langue)
	REFERENCES public.langue(code),

  CONSTRAINT texte_pkey PRIMARY KEY (id, langue)
)

\COPY texte FROM '/tmp/81-texte.csv' DELIMITER ',' CSV HEADER QUOTE '"' ESCAPE '''' ENCODING 'UTF8';
