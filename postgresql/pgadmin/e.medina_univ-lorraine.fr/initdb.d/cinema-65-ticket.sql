\connect iutsd;

CREATE TABLE cinema.ticket (
  id uuid DEFAULT gen_random_uuid() NOT NULL,
  seance integer,
  prix money,
  creation timestamp without time zone DEFAULT now(),
  serial integer NOT NULL,
  signature character varying(80)
);

CREATE SEQUENCE cinema.ticket_serial_seq
  AS integer
  START WITH 1
  INCREMENT BY 1
  NO MINVALUE
  NO MAXVALUE
  CACHE 1;

ALTER TABLE ONLY cinema.ticket ALTER COLUMN serial SET DEFAULT nextval('cinema.ticket_serial_seq'::regclass);

ALTER TABLE IF EXISTS cinema.ticket
  ADD CONSTRAINT ticket_seance_fk FOREIGN KEY (seance)
  REFERENCES cinema.seance (id) MATCH SIMPLE
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
  NOT VALID;

CREATE INDEX fki_ticket_seance_fk
  ON cinema.ticket(seance);

\COPY cinema.ticket FROM '/tmp/65-ticket.csv' DELIMITER ',' CSV HEADER QUOTE '"' ESCAPE '''' ENCODING 'UTF8';
