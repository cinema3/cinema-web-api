\connect iutsd;

CREATE SCHEMA cinema AUTHORIZATION iutsd;

CREATE TABLE cinema.cinema (
  "id" integer PRIMARY KEY,
  "nom" character varying(60),
  "adresse" adresse
);

\COPY cinema.cinema FROM '/tmp/cinema.csv' DELIMITER ',' CSV HEADER QUOTE '"' ESCAPE '''' ENCODING 'UTF8';

CREATE TABLE cinema.societe
(
    id uuid DEFAULT gen_random_uuid() NOT NULL PRIMARY KEY,
    nom character varying(35)
);

\COPY cinema.societe FROM '/tmp/cinema-societe.csv' DELIMITER ',' CSV HEADER QUOTE '"' ESCAPE '''' ENCODING 'UTF8';
