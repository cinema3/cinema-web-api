CREATE SCHEMA bd;

CREATE TABLE bd.serie
(
  id integer NOT NULL PRIMARY KEY,
  serie character varying(30) COLLATE NOT NULL,
  CONSTRAINT serie_unique UNIQUE (serie)
)

\COPY bd.serie FROM '/tmp/31-serie.csv' DELIMITER ',' CSV HEADER QUOTE '"' ESCAPE '''' ENCODING 'UTF8';

CREATE TABLE bd.album
(
  id integer NOT NULL PRIMARY KEY,
  titre character varying(30) NOT NULL,
  tome character varying(4),
)
