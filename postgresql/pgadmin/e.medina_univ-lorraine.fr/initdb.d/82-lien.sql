CREATE TABLE IF NOT EXISTS public.lien
(
  id uuid NOT NULL,
  site character varying(20) NOT NULL,
  identifiant character varying(16),

  CONSTRAINT lien_personne_fk FOREIGN KEY (id)
	REFERENCES personne(id),

  CONSTRAINT lien_pk PRIMARY KEY (id, site)
)

\COPY lien FROM '/tmp/82-lien.csv' DELIMITER ',' CSV HEADER QUOTE '"' ESCAPE '''' ENCODING 'UTF8';
