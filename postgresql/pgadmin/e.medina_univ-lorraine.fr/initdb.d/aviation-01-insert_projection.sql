-- DROP FUNCTION aviation.insert_projection();

CREATE FUNCTION aviation.insert_projection()
  RETURNS trigger
  LANGUAGE 'plpgsql'
  COST 100
  VOLATILE NOT LEAKPROOF
AS $BODY$
DECLARE
  srid integer := nextval('aviation.srid_seq');
BEGIN

INSERT INTO spatial_ref_sys (srid, auth_name, proj4text) VALUES (srid, CONCAT('azimuthal equidistant ', NEW.ville), CONCAT('+proj=aeqd +lat_0=', ST_Y(NEW.coordonnees), ' +lon_0=', ST_X(NEW.coordonnees), ' +x_0=0 +y_0=0 +a=6371000 +b=6371000 +units=m +no_defs') );

NEW.srid := srid;

RETURN NEW;

END
$BODY$;

ALTER FUNCTION aviation.insert_projection()
  OWNER TO iutsd;
