\connect iutsd;

CREATE TABLE cinema.film (
  id uuid DEFAULT gen_random_uuid() NOT NULL,
  titre character varying(80) NOT NULL,
  titre_original character varying(80),
  annee integer,
  sortie date,
  duree integer,
  CONSTRAINT film_pkey PRIMARY KEY (id)
);

\COPY cinema.film FROM '/tmp/30-film.csv' DELIMITER ',' CSV HEADER QUOTE '"' ESCAPE '''' ENCODING 'UTF8';
