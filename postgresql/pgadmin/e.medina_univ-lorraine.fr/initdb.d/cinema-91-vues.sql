CREATE VIEW cinema.seance_cinema AS
 SELECT m.titre,
    m.duree,
    t.nom,
    s.seance
   FROM (((cinema.seance s
     JOIN cinema.salle c ON ((s.salle = c.id)))
     JOIN cinema.cinema t ON ((c.cinema = t.id)))
     JOIN cinema.film m ON ((s.film = m.id)));
