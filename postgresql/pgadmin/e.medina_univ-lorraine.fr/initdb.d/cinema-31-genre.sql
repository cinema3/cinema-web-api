CREATE TABLE cinema.genre
(
  id integer NOT NULL,
  genre character varying(20) NOT NULL,
  PRIMARY KEY (id)
);

\COPY cinema.genre FROM '/tmp/31-genre.csv' DELIMITER ',' CSV HEADER QUOTE '"' ESCAPE '''' ENCODING 'UTF8';
