-- DROP FUNCTION aviation.route_grand_cercle();

CREATE FUNCTION aviation.route_grand_cercle()
  RETURNS trigger
  LANGUAGE 'plpgsql'
  COST 100
  VOLATILE NOT LEAKPROOF
AS $BODY$
DECLARE
  dateline geometry;
  route geometry;
  intersec geometry;
  frac float := 0;
  aeqd_srid integer;
BEGIN

--RAISE INFO 'start % %', NEW.aeroport_origine, NEW.aeroport_destination;

--DELETE FROM spatial_ref_sys WHERE srid = '99999';
--INSERT INTO spatial_ref_sys (srid, auth_name, proj4text) VALUES ('99999', 'azimuthal equidistant', (SELECT CONCAT('+proj=aeqd +lat_0=', ST_Y(localisation), ' +lon_0=', ST_X(localisation), ' +x_0=0 +y_0=0 +a=6371000 +b=6371000 +units=m +no_defs' ) FROM aviation.aeroports WHERE code_iata = NEW.aeroport_origine));

--RAISE INFO 'set srid : done';

SELECT srid FROM aviation.aeroport WHERE code_iata = NEW.aeroport_origine INTO aeqd_srid;

dateline := ST_Transform(ST_Segmentize(ST_GeomFromText('LINESTRING(180 90, 180 -90)', 4326),5), aeqd_srid);

--RAISE INFO 'dateline : %', ST_AsText(dateline);

SELECT ST_MakeLine(ST_Transform(a.coordonnees, aeqd_srid), ST_Transform(b.coordonnees, aeqd_srid))
FROM
  aviation.aeroport a, aviation.aeroport b
WHERE
 (a.code_iata = NEW.aeroport_origine) AND
 (b.code_iata = NEW.aeroport_destination)
INTO route;

--RAISE INFO 'route : %', ST_AsText(route);

intersec := ST_GeometryN(ST_INTERSECTION(route, dateline), 1);

--RAISE INFO 'intersec : %', ST_AsText(intersec);

IF intersec IS NULL THEN
	NEW.route := ST_Transform(ST_Segmentize(route, 50000), 4326);
ELSE
  frac := ST_LineLocatePoint(route, intersec);
  --RAISE INFO 'frac : %', frac;
  IF frac > 0 AND  frac < 1 THEN
		NEW.route := ST_Transform(ST_Segmentize(ST_UNION(ST_LineSubstring(route,0.0, frac-0.01), ST_LineSubstring(route,frac+0.01, 1.0)),50000),4326);
	ELSE
		NEW.route := ST_Transform(ST_Segmentize(route, 50000), 4326);
	END IF;
END IF;

--RAISE INFO 'new.route : done';

RETURN NEW;

END
$BODY$;

ALTER FUNCTION aviation.route_grand_cercle()
  OWNER TO iutsd;
