CREATE SCHEMA aviation;

CREATE SEQUENCE IF NOT EXISTS aviation.srid_seq
  INCREMENT 1
  START 100000
  MINVALUE 1
  MAXVALUE 9223372036854775807
  CACHE 1;

CREATE TABLE IF NOT EXISTS aviation.aeroport
(
  code_icao character(4) PRIMARY KEY,
  code_iata character(3) NOT NULL,
  nom character varying(50),
  ville character(50),
  pays character varying(2),
  altitude integer,
  coordonnees geometry,
  srid integer
);

CREATE TRIGGER trigger_aeroport_insert
  BEFORE INSERT
  ON aviation.aeroport
  FOR EACH ROW
  EXECUTE FUNCTION aviation.insert_projection();

CREATE TRIGGER trigger_aeroport_insert
  BEFORE UPDATE
  ON aviation.aeroport
  FOR EACH ROW
  EXECUTE FUNCTION aviation.update_projection();

\COPY aviation.aeroport (code_icao, code_iata, nom, ville, pays, altitude, coordonnees) FROM '/tmp/aeroport.csv' DELIMITER ',' CSV HEADER QUOTE '"' ESCAPE '''' ENCODING 'UTF8';
