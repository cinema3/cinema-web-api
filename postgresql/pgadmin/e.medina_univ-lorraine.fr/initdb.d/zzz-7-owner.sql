SELECT format('ALTER TABLE IF EXISTS %I.%I OWNER TO iutsd;', table_schema, table_name)
FROM information_schema.tables
WHERE table_schema IN ('cinema', 'musique', 'livre', 'aviation') \gexec

SELECT format('ALTER TABLE IF EXISTS %I.%I OWNER TO iutsd;', sequence_schema, sequence_name)
FROM information_schema.sequences
WHERE sequence_schema IN ('cinema', 'musique', 'livre') \gexec

SELECT format('ALTER TABLE IF EXISTS %I.%I OWNER TO iutsd;', schemaname, matviewname)
FROM pg_matviews
WHERE schemaname IN ('cinema', 'musique', 'livre') AND matviewowner = 'postgres' \gexec

SELECT format('REFRESH MATERIALIZED VIEW %I.%I WITH DATA;', schemaname, matviewname)
FROM pg_matviews
WHERE schemaname IN ('cinema', 'musique', 'livre') AND NOT ispopulated \gexec
