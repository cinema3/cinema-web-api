CREATE MATERIALIZED VIEW cinema.acteur AS
  SELECT p.id,
  CASE
    WHEN (p.artiste IS NOT NULL) THEN (p.artiste)::text
    ELSE (((p.prenom)::text || ' '::text) || (p.nom)::text)
  END AS name,
  p.naissance,
  CASE
    WHEN (p.deces IS NULL) THEN date_part('year'::text, age((p.naissance)::timestamp with time zone))
    ELSE NULL::double precision
  END AS age,
  p.nationalites,
  count(c.id) AS nb_film
  FROM (cinema.equipe c
  JOIN personne p ON ((c.personne = p.id)))
  WHERE ((c.role)::text = 'actor'::text)
  GROUP BY p.id
WITH NO DATA;

CREATE MATERIALIZED VIEW bd.auteur AS
  SELECT p.id,
  CASE
    WHEN (p.artiste IS NOT NULL) THEN (p.artiste)::text
    ELSE (((p.prenom)::text || ' '::text) || (p.nom)::text)
  END AS name,
  p.naissance,
  CASE
    WHEN (p.deces IS NULL) THEN date_part('year'::text, age((p.naissance)::timestamp with time zone))
    ELSE NULL::double precision
  END AS age,
  p.nationalites,
  count(c.id) AS nb_film
  FROM (bd.equipe c
  JOIN personne p ON ((c.personne = p.id)))
  WHERE ((c.role)::text IN  ('actor'::text))
  GROUP BY p.id
WITH NO DATA;
