\connect iutsd;

CREATE EXTENSION IF NOT EXISTS postgis;

-- User iutsd

CREATE ROLE iutsd WITH
  LOGIN
  NOSUPERUSER
  NOCREATEDB
  NOCREATEROLE
  INHERIT
  NOREPLICATION
  CONNECTION LIMIT -1
  PASSWORD 'lFFyZmfwxyfF';

ALTER DATABASE iutsd OWNER TO iutsd;

-- FusionAuth

CREATE ROLE fusionauth WITH
  LOGIN
  NOSUPERUSER
  NOCREATEDB
  NOCREATEROLE
  INHERIT
  NOREPLICATION
  CONNECTION LIMIT -1
  PASSWORD '2sVhvnpkvPS3';

CREATE DATABASE fusionauth
  WITH
  OWNER = fusionauth
  CONNECTION LIMIT = -1
  IS_TEMPLATE = False;

--


-- CREATE SCHEMA livre;

-- Adresse

CREATE TYPE public.adresse AS (
	voie character varying(60),
	codepostal character varying(8),
	ville character varying(40),
	coordonnees public.geometry
);

-- Pays

CREATE TABLE pays (
  "code-2" character varying(2) NOT NULL PRIMARY KEY,
  "code-3" character varying(3) NOT NULL,
  "code-num" integer NOT NULL,
  "nom" character varying(45) NOT NULL,
  "drapeau" character(2)
);

COMMENT ON COLUMN public.pays."code-2"
  IS 'iso 3166-1 alpha 2';

COMMENT ON COLUMN public.pays."code-3"
 IS 'iso 3166-1 alpha 3';

COMMENT ON COLUMN public.pays."code-num"
 IS 'iso 3166-1 numeric';

ALTER TABLE IF EXISTS pays OWNER TO iutsd;

\COPY pays FROM '/tmp/public-pays.csv' DELIMITER ',' CSV HEADER QUOTE '"' ESCAPE '''' ENCODING 'UTF8';

-- Langue

CREATE TABLE langue
(
  code character(3) NOT NULL PRIMARY KEY,
  langue character varying(20),
  "français" character varying(20)
);

ALTER TABLE IF EXISTS langue OWNER TO iutsd;

\COPY langue FROM '/tmp/public-langue.csv' DELIMITER ',' CSV HEADER QUOTE '"' ESCAPE '''' ENCODING 'UTF8';
