CREATE VIEW cinema.adm_film_sans_equipe AS
  SELECT
    m.id,
    m.titre
  FROM (cinema.film m
  LEFT JOIN cinema.equipe c ON ((m.id = c.film)))
  WHERE (c.id IS NULL);
