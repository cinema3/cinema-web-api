Web Service pour les travaux pratiques de l'IUT de Saint-Dié des Vosges

```env
# PGADMIN
PGADMIN_DEFAULT_EMAIL=votre_email@domaine.fr
PGADMIN_DEFAULT_PASSWORD=mot_de_passe_administration

# POSTGRESQL
POSTGRES_DB=iutsd
POSTGRES_USER=postgres
POSTGRES_PASSWORD=mot_de_passe_postgres

# POSTGRAPHILE
DATABASE_URL=postgres://iutsd:mot_de_passe_iutsd@postgresql:5432/iutsd
DATABASE_SCHEMA=cinema
```

### Express

```env
PORT=80
POSTGRES_HOST=postgresql
POSTGRES_PORT=5432
POSTGRES_DB=iutsd
POSTGRES_USER=iutsd
POSTGRES_PASSWORD=mot_de_passe_postgres
```


SELECT ST_Point( 7, 47, 4326);

https://www.youtube.com/watch?v=J0nbgXIarhc
https://vfr-pilote.fr/aerodrome/lfgy/
https://www.iso.org/obp/ui/#iso:code:3166:GD


https://www.airfleets.fr/ficheapp/plane-a350-354.htm
https://fr.radarbox.com/data/registration/3b-ncf/1845289933
