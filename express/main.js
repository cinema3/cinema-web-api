module.exports = () => {

  const router = require('express').Router();

  router.get('/', (req, res) => {
    res.status(200).send({
      description: "Application Express",
      version: "2023.01.20"
    });
  });

  return router;
}
