module.exports = (db) => {

  const router = require('express').Router();
  const validator = require('validator');

  router.get('/pilote', async (req, res) => {
    const data = await db.any("SELECT * from aviation.pilote ORDER BY nom desc")
    res.status(200).json(data);
  });

  router.get('/depart', async (req, res) => {
    const data = await db.any(`SELECT DISTINCT route.aeroport_origine,
    aeroport.nom,
    aeroport.ville
    FROM aviation.route
    JOIN aviation.aeroport ON aeroport.code_iata = route.aeroport_origine;`)
    res.status(200).json(data);
  });

  router.get('/arrivee', async (req, res) => {
    const data = await db.any("SELECT * from aviation.pilote ORDER BY nom desc")
    res.status(200).json(data);
  });

  return router;
}
