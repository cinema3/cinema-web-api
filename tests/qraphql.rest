POST http://localhost:8001/graphql
Content-Type: application/json
Accept: application/json
X-REQUEST-TYPE: GraphQL

{
  allActeurs(first: 10) {
    edges {
      node {
        id
        name
        naissance
        age
        nbFilm
      }
    }
  }
}
